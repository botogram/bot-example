f = (...args) => console.log(...args)

const config = require('./config')
Config = new config()


_ = require('lodash')
I18n = require('telegraf-i18n')

const path = require('path')

const Telegraf = require('telegraf')
const TelegrafFlow = require('telegraf-flow')
const RateLimit = require('telegraf-ratelimit')

const {Scene, enter} = TelegrafFlow
const {Extra, Markup} = Telegraf

const RedisSession = require('telegraf-session-redis')

const db = require('./db')

const User = require('./middlewares/user')
const Keyboard = require('./middlewares/keyboard')
const Info = require('./middlewares/info')

const user = new User()
const keyboard = new Keyboard()
const info = new Info({
  defaultLocale: 'ru',
  directory: path.resolve(__dirname, 'info')
})

bot = new Telegraf(Config.bot.token)

bot.use((ctx, next) => {
  const start = new Date()
  return next().then(() => {
    const ms = new Date() - start
    console.log('response time %s - %s - %s', ms, ctx.from.first_name, ctx.updateType)
  })
})


const session = new RedisSession({
  store: {
    host: '127.0.0.1',
    port: 6379,
    db: 3
  }
})


// // Set limit to 1 message per 3 seconds
// const limiter = new RateLimit({
//   window: 1000,
//   limit: 2,
//   onLimitExceeded: (ctx, next) => ctx.reply('Rate limit exceeded. Limit to 1 message per 1 seconds')
// })

// bot.use(limiter.middleware())

bot.context.db = db


// Add middleware
i18n = new I18n({
  directory: path.resolve(__dirname, 'locales')
})

bot.use(session.middleware())
bot.use(i18n.middleware())
bot.use(user.middleware())
bot.use(keyboard.middleware())
bot.use(info.middleware())



// Set locale to `en`
bot.command('/en', (ctx) => {
  ctx.i18n.locale('en')
  return ctx.reply(ctx.i18n.t('greeting', {
    username: ctx.from.username
  }))
})

// Set locale to `ru`
bot.command('/ru', (ctx) => {
  ctx.i18n.locale('ru')
  return ctx.reply(ctx.i18n.t('greeting', {
    username: ctx.from.username
  }))
})



flow = new TelegrafFlow()

// Global commands
flow.command(['start', 'help'], (ctx) => {
  ctx.flow.leave()
  return ctx.reply(ctx.i18n.t('greeting'), ctx.keyboard.menu(ctx))
})

flow.hears(I18n.match('keyboard.menu.change_language'), (ctx) => {
  ctx.i18n.locale(ctx.i18n.locale() == 'en' ? 'ru' : 'en')
  return ctx.reply('👌🏻', ctx.keyboard.menu(ctx))
})

flow.hears([I18n.match('keyboard.menu.channels'), I18n.match('keyboard.menu.settings')], (ctx) => {
  ctx.flow.leave()
  return ctx.reply('👌🏻', ctx.keyboard.menu(ctx))
})


flow.hears(I18n.match('keyboard.menu.create_post'), enter('post'))

flow.command('admin', enter('admin'))


// Scene registration
flow.register(require('./scenes/admin'))
flow.register(require('./scenes/post'))

bot.use(flow.middleware())

bot.telegram.getMe().then((botInfo) => {
  bot.options.username = botInfo.username
  console.log('@' + botInfo.username + ' was start.')
})




bot.catch((err) => {
  console.log('Ooops', err)
})

bot.startPolling(30)