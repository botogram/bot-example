const path = require('path')
const fs = require('fs')
const yaml = require('js-yaml')
const request = require('request-promise')
const pEachSeries = require('p-each-series')

const Telegraf = require('telegraf')
const {Extra, Markup} = Telegraf
const TelegrafFlow = require('telegraf-flow')
const {Scene, WizardScene, enter} = TelegrafFlow

const admin = new Scene('admin')

const fileDirectories = {
  config: path.resolve(__dirname, '..', 'info'),
  localization: path.resolve(__dirname, '..', 'locales')
}

admin.use((ctx, next) => {
  if (Config.root.indexOf(ctx.from.id) == -1) {
    ctx.flow.leave()
    return ctx.reply('Access not found.')
  }
  return next()
})

bot.use((ctx, next) => {
  ctx.keyboard.admin = {}
  ctx.keyboard.admin.menu = (ctx) => Markup.keyboard([
    [Markup.button(ctx.i18n.t('keyboard.admin.newsletter.start'))],
    [Markup.button(ctx.i18n.t('keyboard.admin.files.start'))]
  ]).resize().extra()

  return next()
})



flow.hears([I18n.match('keyboard.admin.home'), '/back'], (ctx) => ctx.flow.enter('admin'))

admin.enter(ctx => {
  return ctx.reply(ctx.i18n.t('admin.welcome'), ctx.keyboard.admin.menu(ctx))
})

admin.hears(I18n.match('keyboard.admin.newsletter.start'), (ctx) => {
  let keyboard = _.chunk(_.map(ctx.info.config.languages, 'name'), 2)
  keyboard.unshift([ctx.i18n.t('keyboard.admin.newsletter.all')])
  ctx.session.newsletter = {
    posts: [], 
    find: {
      is_blocked: false
    }
  }
  ctx.flow.enter('newsletter')
  return ctx.reply(ctx.i18n.t('admin.newsletter.select_lang'), Markup
    .keyboard(keyboard)
    .resize()
    .extra()
  )
})

const newsletter = new WizardScene('newsletter',
  (ctx) => {
    if (
      ctx.message.text == ctx.i18n.t('keyboard.admin.newsletter.all') || 
      _.map(ctx.info.config.languages, 'name').includes(ctx.message.text)
    ) {
      if (ctx.message.text != ctx.i18n.t('keyboard.admin.newsletter.all')) {
        Object.assign(ctx.session.newsletter.find, {
          lang: _.find(ctx.info.config.languages, {
            name: ctx.message.text
          }).code
        })
      }
      ctx.flow.wizard.next()
      return ctx.reply(ctx.i18n.t('admin.newsletter.send'), Markup.hideKeyboard().extra())
    }
  },
  (ctx) => {
    if (ctx.message.text == '/done') {
      ctx.flow.wizard.next()
      return ctx.reply(ctx.i18n.t('admin.newsletter.done'))
    }
    
    const error = (error) => {
      return ctx.replyWithHTML(ctx.i18n.t('admin.newsletter.error_sent', {error: error.description}))
    }
    const ok = () => {
      ctx.session.newsletter.posts.push(ctx.message)
      return ctx.reply(ctx.i18n.t('admin.newsletter.send_done'))
    }
    
    if (ctx.message.forward_from_chat) {
      return ctx.telegram.forwardMessage(ctx.from.id, ctx.message.from.id, ctx.message.message_id)
        .then(ok)
        .catch(error)
    } else {
      return ctx.telegram.sendCopy(ctx.from.id, ctx.message, {parse_mode: 'html'})
        .then(ok)
        .catch(error)
    }
  },
  (ctx) => {
    if (ctx.message.text != '/yes') {
      return
    }
    f(ctx.session.newsletter.find)
    f('start')
    return ctx.db.users.find(ctx.session.newsletter.find).select('uid first_name')
      .then(users => {
        f(users.length)
        return pEachSeries(users, (user) => {
          f(user)
          return pEachSeries(ctx.session.newsletter.posts, (post) => {
            const error = e => {
              if (e.description == 'Bot was blocked by the user') {
                user.is_blocked = true
                return user.save()
              } else {
                f(e)
                return user
              }
            }
            if (post.forward_from_chat) {
              return ctx.telegram.forwardMessage(user.uid, post.from.id, post.message_id)
                .catch(error)
            } else {
              return ctx.telegram.sendCopy(user.uid, post, {parse_mode: 'html'})
                .catch(error)
            }
          }).then((result) => {
            f('sent ' + user.first_name)
          })
        })
      })
      .then(result => f('done', result))
  }
)


admin.hears(I18n.match('keyboard.admin.files.start'), (ctx) => {
  return ctx.reply(ctx.i18n.t('admin.files.option'), Markup.inlineKeyboard([
    [Markup.callbackButton(ctx.i18n.t('keyboard.admin.files.option_config'), 'files_option_config')],
    [Markup.callbackButton(ctx.i18n.t('keyboard.admin.files.option_localization'), 'files_option_localization')]
  ]).extra())
})

admin.action(['files_option_localization', 'files_option_config'], (ctx) => {
  ctx.session.admin_files_option = ctx.match.split('_').pop()

  return ctx.reply(ctx.i18n.t('admin.files.type'), Markup.keyboard([
    Markup.button(ctx.i18n.t('keyboard.admin.files.type_load')),
    Markup.button(ctx.i18n.t('keyboard.admin.files.type_upload'))
  ]).resize().extra()).then(() => ctx.answerCallbackQuery())
})


admin.hears(I18n.match('keyboard.admin.files.type_load'), (ctx) => {
  const option = ctx.session.admin_files_option
  const directory = fileDirectories[option]
  const context = {option, directory}
  const calls = []

  if (!directory || !fs.existsSync(directory)) {
    return ctx.reply(ctx.i18n.t('admin.files.directory_not_found', context))
  }

  const files = fs.readdirSync(directory)
  if (!files.length) {
    return ctx.reply(ctx.i18n.t('admin.files.not_found', context))
  }
  files.forEach((fileName) => {
    calls.push(ctx.telegram.sendDocument(ctx.from.id, {
      source: path.resolve(directory, fileName)
    }))
  })

  return Promise.all(calls).then((result) => ctx.reply(ctx.i18n.t('admin.files.sent')))
})

admin.hears(I18n.match('keyboard.admin.files.type_upload'), enter('upload_files'))


const upload = new Scene('upload_files')

upload.enter((ctx) => {
  return ctx.replyWithHTML(ctx.i18n.t('admin.files.upload', {
    option: ctx.session.admin_files_option
  }), Markup.keyboard([[ctx.i18n.t('keyboard.admin.files.upload_cancel')]]).resize().extra())
})

upload.hears(I18n.match('keyboard.admin.files.upload_cancel'), enter('admin'))


upload.on('document', (ctx) => {
  const document = ctx.message.document
  const directory = fileDirectories[ctx.session.admin_files_option]
  if (!['.yaml', '.yml'].includes(path.extname(document.file_name))) {
    return ctx.reply(ctx.i18n.t('admin.files.upload_bad_ext'))
  }
  return ctx.telegram.getFileLink(document.file_id)
    .then(request)
    .then((data) => {
      try {
        yaml.safeLoad(data)
      } catch (error) {
        return ctx.reply(ctx.i18n.t('admin.files.upload_yaml_exception', { error }), Extra.HTML())
      }
      
      if (!directory && !fs.existsSync(directory)) {
        return ctx.reply(ctx.i18n.t('admin.files.directory_not_found'))
      }
      
      fs.writeFileSync(path.join(directory, '/', document.file_name), data, 'utf-8')
      
      if (ctx.session.admin_files_option == 'localization') {
        i18n.loadLocales(directory)
      }
      
      return ctx.reply(ctx.i18n.t('admin.files.upload_ok', {
        file_name: document.file_name
      }), Extra.HTML())
  })
})


flow.register(upload)

flow.register(newsletter)


module.exports = admin