const Telegraf = require('telegraf')
const {Extra, Markup} = Telegraf
const TelegrafFlow = require('telegraf-flow')
const {Scene, enter} = TelegrafFlow

const post = new Scene('post')

post.enter((ctx) => {
  return ctx.reply(ctx.i18n.t('post.welcome'), ctx.keyboard.post.menu(ctx))
})



post.hears(I18n.match('keyboard.post.cancel'), (ctx) => {
  ctx.flow.leave()
  return ctx.reply('👌🏻', ctx.keyboard.menu(ctx))
})
post.on('text', (ctx) => ctx.reply(ctx.message.text))
post.on('message', (ctx) => ctx.reply('Only text messages please'))


module.exports = post