f = (...args) => console.log(...args)

exports.Telegraf = require('telegraf')
exports.TelegrafFlow = require('telegraf-flow')

exports.Scene = TelegrafFlow.Scene
exports.enter = TelegrafFlow.enter

exports.Extra = Telegraf.Extra
exports.Markup = Telegraf.Markup

const Config = require('./config')()


exports.bot = new Telegraf(Config.bot.token)