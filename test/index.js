const { bot } = require('./test/bot')

bot.use((ctx, next) => {
  const start = new Date()
  return next().then(() => {
    const ms = new Date() - start
    console.log('response time %s - %s - %s', ms, ctx.from.first_name, ctx.updateType)
  })
})

bot.startPolling(30)