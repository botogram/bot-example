const mongoose = require('mongoose')

mongoose.Promise = global.Promise

const Schema = mongoose.Schema
const connection = mongoose.connect(Config.mongo)

const users = new Schema({
  uid: {
    type: Number,
    unique: true,
    index: true
  },
  username: String,
  first_name: String,
  last_name: String,
  created_at: Date,
  updated_at: Date,
  is_subscribed: {
    type: Boolean,
    default: true
  },
  is_blocked: {
    type: Boolean,
    default: false
  },
  lang: String
})

users.pre('save', function (next) {
  if (this.created_at == null) {
    this.created_at = new Date()
  }
  this.updated_at = new Date()
  return next()
})

module.exports = {
  users: mongoose.model('User', users)
}