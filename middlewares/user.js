class User {
  constructor() {

  }

  middleware() {
    return ((ctx, next) => {
      if (ctx.chat.title) {
        return next()
      }
      return ctx.db.users.findOne({
        uid: ctx.from.id
      }).then((user) => {
        if (user) {
          const info = {}
          for (let data of ['first_name', 'last_name', 'username']) {
            if (user[data] != ctx.from[data]) {
              info[data] = ctx.from[data] || ''
            }
            if (!ctx.from[data] && user[data]) {
              info[data] = undefined
            }
          }
          if (user.is_blocked) {
            info.is_blocked = false
          }
          if (_.size(info)) {
            Object.assign(user, info)
            return user.save()
          } else {
            return user
          }
        } else {
          return ctx.db.users.create({
            uid: ctx.from.id,
            first_name: ctx.from.first_name,
            last_name: ctx.from.last_name,
            username: ctx.from.username,
            lang: ctx.i18n.locale()
          })
        }
      }, (e) => {
        console.log('Error find user: ', e)
      }).then((user) => {
        ctx.user = user
        return next()
      })
    })
  }
}

module.exports = User