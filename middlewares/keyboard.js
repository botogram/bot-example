const Telegraf = require('telegraf')
const {Extra, Markup} = Telegraf

class Keyboard {
  constructor() {

  }

  middleware() {
    return ((ctx, next) => {
      ctx.keyboard = {}
      ctx.keyboard.menu = (ctx) => Markup.keyboard([
        [ctx.i18n.t('keyboard.menu.create_post')],
        [ctx.i18n.t('keyboard.menu.channels'), ctx.i18n.t('keyboard.menu.settings')],
        [ctx.i18n.t('keyboard.menu.change_language')]
      ]).resize().extra()

      ctx.keyboard.post = {}
      ctx.keyboard.post.menu = (ctx) => Markup.keyboard([
        [ctx.i18n.t('keyboard.post.cancel')]
      ]).resize().extra()

      ctx.keyboard.admin = {}
      ctx.keyboard.admin.menu = (ctx) => Markup.keyboard([
        [Markup.button(ctx.i18n.t('admin.btn.newsletter'))],
        [Markup.button(ctx.i18n.t('admin.btn.files'))]
      ]).resize().extra()

      return next()
    })
  }
}

module.exports = Keyboard